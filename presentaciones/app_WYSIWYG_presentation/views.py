from django.shortcuts import render
from django.http import HttpResponse
from django.template import RequestContext
from django.shortcuts import render_to_response

def index(request):
    context = RequestContext(request)
    context_dict = {'modulo': "Presentacion" }
    return render_to_response('app_WYSIWYG_presentation/index.html', context_dict, context)
