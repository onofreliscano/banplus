<!DOCTYPE html>

<html class="no-js" lang="es">
<head>
    <title>Agilidad en el Contexto de RRHH - PDVSA AIT</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="">
<meta name="viewport" content="width=800, user-scalable=no">





</head>



    <body class="impress-not-supported" >

<?php include '../includes/css-js.html';?>
<?php include '../includes/header_presentation.html';?>




<div id="impress">

  <div id="CVP" class="step" data-x="1000" data-y="2000" data-scale="2"><br>
    <span class="fonts_title_presentation_main3">
        Corporación Venezolana del Petróleo (CVP)<br>
    </span>
    <span class="datosOnofre">
      <ol>
    <li> </li>
    <li><br> en conjunto con: <br>
      <b>Dirección de Automatización, Informática y Telecomunicaciones (AIT)</b></li>

    </ol>

    </span>
        <br>



    </div>


    <div id="titulo" class="step" data-x="1200" data-y="2300" data-z="-100" data-rotate-x="-40" data-rotate-y="10" data-scale="2">
        <span class="fonts_title_presentation_main">Agilidad en el contexto<br>
        de Recursos Humanos
        </span>

    </div>


<!----------------- SLIDE #1 ------------------>

   <div class="step" data-x="1000" data-y="1000" data-scale="4.5">

        <span class="fonts_title_presentation_main2">
            Onofre Liscano<br>
        </span>
        <span class="datosOnofre">
          <ol>
  <li> Analista de Planificación Control y Gestión (CVP)</li>
  <li> Líder funcional del proyecto de reingeniería del Sistema Integrado de Portafolio y Oportunidades (AIT)</li>

</ol>

        </span>
            <br>

            <table style="width:80%">

                <tr>
                    <td><a href="https://www.facebook.com/onofreliscano" target="_blank"><img src="../../static/ref/images/logo-fb.jpg" width="40" height="40"></a></td>
                    <td><a href="https://twitter.com/onofreliscano" target="_blank"><img src="../../static/ref/images/logo-tw.jpg" width="40" height="40"></a></td>
                    <td><a href="https://ve.linkedin.com/in/onofre-liscano-95189319" target="_blank"><img src="../../static/ref/images/logo-in.jpg" width="40" height="40"></a></td>
                    <td><a href="https://plus.google.com/+onofreliscano" target="_blank"><img src="../../static/ref/images/logo-g.jpg" width="40" height="40"></a></td>
                    <td><span class="datosOnofreSocial">onofreliscano / LISCANOO</span></td>
                </tr>

              </table>




    </div>

<!----------------- SLIDE #2 ------------------>
<!--<a href="../../static/downloads/Presentacion_SIGRH.pdf" class="fonts_link_slide_pdvsa" target="_blank">Descargar presentación en formato PDF</a>-->
<!--
    <div class="step" data-x="0" data-y="400" data-scale="0.5" data-rotate="90" >
        <center>
        <span class="fonts_title_presentation_main">
             Plan de Jubilación. II Semestre 2017
             CVP y Empresas Mixtas<br>
             Gerencia de RRHH CVP
        </span><br>

        </center>
    </div>-->

<div class="step" data-x="0" data-y="400" data-scale="1" data-rotate="90" >
        <center>
        <span class="fonts_title_presentation_main">
             Plan de Jubilación. II Semestre 2017
             CVP y Empresas Mixtas<br>
             Gerencia de RRHH CVP
        </span><br>

        </center>
    </div>

<!----------------- SLIDE #3 ------------------>

    <div id = "INFO" class="step slide_PDVSA" data-x="0" data-y="-1500">
 <p align="justify">
        <span class="fonts_content_presentation_main">
         <br>
            <br>
        <strong>Objetivo</strong>
            <br>
            <br>

               Informar a la Junta Directiva de la CVP, la proyección del II Semestre de los trabajadores de la CVP
               y Empresas Mixtas que al alcanzar la edad de retiro cumplirán con los criterios de elegibilidad para
               hacerse acreedores al beneficio de jubilación, conforme con lo establecido en la norma y disposiciones del Plan.


            </span>
            </p>
    </div>

<!----------------- SLIDE #2 ------------------>

    <div id = "inquitudes" class="step slide_PDVSA " data-x="1000" data-y="-1500">

    <p align="justify">
        <span class="fonts_content_presentation_main">

            <br>
        <strong>Antecedentes</strong>
            <br>
            <br>

               Desde que CVP asumió el proceso de Jubilación se establecieron la líneas de acción para dar estricto
               cumplimiento al Plan de Jubilación de los trabajadores de la CVP y de las Empresas Mixtas  inmediatamente
               alcance la edad de retiro, años de servicios y aportes al CCI, conforme a los criterios de elegibilidad
               establecidos en las normativas internas.

            </span>
            </p>

    </div>



<!--
     <div id="CVP" class="step slide_PDVSA" data-x="44000" data-y="-1500">
    CVP a nivel nacional<br>
        Detalle de jubilable(s):

    <?php
    if ($count99 == 1) {
        echo $count99 . " trabajador (a)";  }
    else{
        echo $count99 . " trabajadores (as)";  }
        ?>


        <br><br>
        <iframe id ="myChart" width="100%" height="69%" src="jubilables.php?id=7" scrolling="yes">
         <p>Your browser does not support iframes.</p>
        </iframe>
        <br>
        <table width="100%">
            <tr>
                <td width="95%"></td>
                <td><a href="#CVPMAIN"><img src="../../static/images/back.png" width="30" height="30"> </a></td>
            </tr>
        </table>
    </div>

-->









<div class="hint">
    <p>Presione espaciadora</p>
</div>
<script>
    if ("ontouchstart" in document.documentElement) {
        document.querySelector(".hint").innerHTML = "<p>Presione las flechas izquierda y/o derecha para retroceder/avanzar</p>";
    }
</script>

<script src="../../static/javascripts/impress.js-master/js/impress.js"></script>
<script>impress().init();</script>

</div>

    <div class="footer_main">

        <?php include '../includes/footer_presentation.html';?>
    </div>

</body>
</html>
