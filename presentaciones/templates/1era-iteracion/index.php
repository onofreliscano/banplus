<!DOCTYPE html>

<html class="no-js" lang="es">
<head>
    <title>Un acercamiento a la transformación digital</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="">
<meta name="viewport" content="width=800, user-scalable=no">





</head>



    <body class="impress-not-supported" >

<?php include '../includes/css-js.html';?>
<?php include '../includes/header_presentation.html';?>
<!--<script src="easytimer.min.js"></script>-->




<div id="impress">

  <div id="CVP" class="step" data-x="1000" data-y="2000" data-scale="2"><br>
    <span class="fonts_title_presentation_main3">
        Banplus<br>
    </span>
    <span class="datosOnofre">
      <ol>
    <li> </li>
    <li><br>A través de:<br>
      <b>La Gerencia de Capacitación y Desarrollo
      <br>
      Vicepresidencia Ejecutiva de Capital Humano</b></li>

    </ol>

    </span>
        <br>



    </div>


    <div id="titulo" class="step" data-x="1200" data-y="2300" data-z="-100" data-rotate-x="-40" data-rotate-y="10" data-scale="2">
        <span class="fonts_title_presentation_main">Un acercamiento a la <br>
        transformación digital
        </span>

    </div>


<!----------------- SLIDE #1 ------------------>

   <div class="step" data-x="1000" data-y="1000" data-scale="4.5">

        <span class="fonts_title_presentation_main2">
            Onofre Liscano<br>
        </span>
        <span class="datosOnofre">


        </span>
            <br>

            <table style="width:80%">

                <tr>
                    <td><a href="https://www.facebook.com/onofreliscano" target="_blank"><img src="../../static/ref/images/logo-fb.jpg" width="40" height="40"></a></td>
                    <td><a href="https://twitter.com/onofreliscano" target="_blank"><img src="../../static/ref/images/logo-tw.jpg" width="40" height="40"></a></td>
                    <td><a href="https://ve.linkedin.com/in/onofre-liscano-95189319" target="_blank"><img src="../../static/ref/images/logo-in.jpg" width="40" height="40"></a></td>
                    <td><a href="https://plus.google.com/+onofreliscano" target="_blank"><img src="../../static/ref/images/logo-g.jpg" width="40" height="40"></a></td>
                    <td><span class="datosOnofreSocial">@onofreliscano</span></td>
                </tr>

              </table>




    </div>

<!----------------- SLIDE #2 ------------------>
<!--<a href="../../static/downloads/Presentacion_SIGRH.pdf" class="fonts_link_slide_pdvsa" target="_blank">Descargar presentación en formato PDF</a>-->
<!--
    <div class="step" data-x="0" data-y="400" data-scale="0.5" data-rotate="90" >
        <center>
        <span class="fonts_title_presentation_main">
             Plan de Jubilación. II Semestre 2017
             CVP y Empresas Mixtas<br>
             Gerencia de RRHH CVP
        </span><br>

        </center>
    </div>-->

<div class="step" data-x="0" data-y="400" data-scale="1" data-rotate="90" >
  <span class="fonts_title_presentation_main3">
      Esta presentación se encuentra disponible en:<br>
  </span>
  <span class="datosOnofre">
    <ol>
<li>Gitlab<br><a href="http://10.0.48.88/sigrh" target="_blank" class="fonts_main_links">http://10.0.48.88/sigrh</a></li><br>
<li>ADA. Ambiente Digital de Aprendizaje:<br><a href="http://ada.pdvsa.com" target="_blank" class="fonts_main_links">http://ada.pdvsa.com</a></li>

</ol>

  </span>
    </div>

<!----------------- SLIDE #0 ------------------>

    <div id = "slide0" class="step slide_PDVSA" data-x="0" data-y="-1500">
        <p align="left">
        <br><br><br>
        <br><br>
        <span class="fonts_title_presentation_main">
        Definición de caos y complejidad<br>
        </span>
        </p>
    </div>

<!----------------- SLIDE #1 ------------------>

    <div id = "slide1" class="step slide_PDVSA " data-x="1000" data-y="-1500">
        <p align="left">
        <span class="fonts_content_presentation_main">
        <br>
        <strong>Entrevista con James Yorke: Teoría del Caos</strong>
        <br><br>
        <video controls id="myvideo" width="420" height="340" controls>
            <source src="../../static/ref/videos/001.mp4"></source>
        </video>
        <br>
        <a href="https://www.cairn.info/revue-management-2002-1-page-31.htm" class="fonts_main_links" target="_blank">Ejemplo 1</a>
        -
        <a href="http://onlinelibrary.wiley.com/doi/10.1177/103841119903600309/full#references" class="fonts_main_links" target="_blank">Ejemplo 2</a>
        -
        <a href="http://tesis.usat.edu.pe/handle/usat/28" class="fonts_main_links" target="_blank">Ejemplo 3</a>
        - <a href="../../static/ref/docs/f001.pdf" class="fonts_main_links" target="_blank">Ejemplo 4</a>

        </span>

        </p>
    </div>

    <!----------------- SLIDE #2 ------------------>

    <div id = "slide2" class="step slide_PDVSA" data-x="2000" data-y="-1500">
        <p align="justify">
          <span class="datosOnofre">
            <ol>
            <li>Libro recomendado: <br>Introducción al pensamiento complejo - E. Morín </li><br>
            <a href="../../static/ref/docs/001.pdf" target="_blank"><img src="../../static/ref/images/001.jpg"></a>
            </ol>
          </span>
        </p>
    </div>

  <!----------------- SLIDE #3 ------------------>
    <div id = "slide3" class="step slide_PDVSA" data-x="3000" data-y="-1500">
      <p align="left">
      <br><br><br>
      <br><br>
      <span class="fonts_title_presentation_main">
      Definición de metodologías ágiles<br>
      Scrum? XP? Kanban?<br>
      Es sólo para desarrolladores?
      </span>
      </p>
    </div>

    <!----------------- SLIDE #4 ------------------>
      <div id = "slide4" class="step slide_PDVSA" data-x="4000" data-y="-1500">
        <p align="left">
          <br>
        <span class="fonts_content_presentation_main">

        <strong>Gestionar la complejidad con Scrum. Por Jerónimo Palacios</strong>
        <a href="../../static/ref/docs/002.pdf" target="_blank"><img src="../../static/ref/images/002.png" width="659" height="422"></a>
        </span>
        <br>
        </p>
      </div>


      <!----------------- SLIDE #5 ------------------>
        <div id = "slide5" class="step slide_PDVSA" data-x="5000" data-y="-1500">
          <p align="left">
          <span class="fonts_content_presentation_main">
          <br>
          <strong>Metodologías ágiles: tendencia y falacia?<br>Un poco de escepticismo positivo</strong>
          <a href="https://hrdailyadvisor.blr.com/2018/02/22/agile-fail-top-challenge-hr-teams-2018/" target="_blank"><img src="../../static/ref/images/002a.png" width="659" height="422"></a>
          </span>
          <br>
          </p>
        </div>


        <!----------------- SLIDE #6 ------------------>
          <div id = "slide6" class="step slide_PDVSA" data-x="6000" data-y="-1500">
            <p align="left">
            <span class="fonts_content_presentation_main"></span>
            <br>
            <strong>Las grandes frustaciones de: <a href="https://www.wikiwand.com/es/Scrum_(desarrollo_de_software)" target="_blank" class="fonts_main_links"> Jeff Sutherland</a></strong>
            <br><br>
            <img src="../../static/ref/images/003.jpg" width="700" height="300">
            <br>
            La grandeza no puede ser impuesta; debe venir de adentro. Pero vive dentro de todos nosotros
            Lectura recomendada:
            <a href="../../static/ref/docs/007.png" target="_blank" class="fonts_main_links"><br>
            The Chaos Report
          </a>






            <br>
            </p>
          </div>

          <!----------------- SLIDE #7 ------------------>
            <div id = "slide7" class="step slide_PDVSA" data-x="7000" data-y="-1500">
              <p align="left">
              <span class="fonts_content_presentation_main"></span>
              <br>
              <strong>'Scrum', 'agile'... <br>así son las nuevas formas de trabajo en BBVA</strong>
              <br><br>
              <video controls id="myvideo" width="420" height="340" controls>
                  <source src="../../static/ref/videos/002.mp4"></source>
              </video>
          </div>

          <!----------------- SLIDE #8 ------------------>
            <div id = "slide8" class="step slide_PDVSA" data-x="8000" data-y="-1500">
              <p align="left"><br>
              <span class="fonts_content_presentation_main"></span>
              <br>
              <strong>Puntos clave de la agilidad en PDVSA CVP: <br><br>
              Una visión diferente para hacer las cosas</strong>
              <br>  <br><br>
              No necesariamente estamos haciendo las cosas mal.
              <br>
              Pero lo que estamos haciendo “bien” podemos hacerlo MEJOR!

          </div>

            <!----------------- SLIDE #9 ------------------>
            <div id = "slide9" class="step slide_PDVSA" data-x="9000" data-y="-1500">
              <p align="left">
              <span class="fonts_content_presentation_main"></span>
              <br>
              <strong>Ejemplo: Experimento con los monos <br></strong>

              <a href="../../static/ref/images/004.png" target="_blank"><img src="../../static/ref/images/004.png" height="370" width="370"></a>

              <br>
              Tomado de: las Reglas no escritas para triunfar en la empresa (informática profesional de Canales Mora) Adictos al trabajo

          </div>

          <!----------------- SLIDE #10 ------------------>
          <div id = "slide10" class="step slide_PDVSA" data-x="10000" data-y="-1500">
            <p align="left">
            <span class="fonts_content_presentation_main"></span>
            <br>
            <img src="../../static/ref/images/005.jpg" height="330" width="330">
            <br><br>
            <strong>"Es más fácil desintegrar un átomo que un prejuicio". </strong>
            <br>
            Albert Einstein.
        </div>

        <!----------------- SLIDE #11 ------------------>
        <div id = "slide11" class="step slide_PDVSA" data-x="11000" data-y="-1500">
          <p align="left">
          <span class="fonts_content_presentation_main"></span>
          <br>
          <strong>La realidad (y retos) de Petróleos de Venezuela</strong>
          <br><br>
          - Complejidad<br>
          - Competitividad<br>
          - Adaptibilidad<br>
          <br><br>
          <strong>Lectura recomendada:</strong><br>
          Digital Darwinisim. Schwartz, Evan
        </div>

        <!----------------- SLIDE #12 ------------------>
        <div id = "slide12" class="step slide_PDVSA" data-x="12000" data-y="-1500">
          <p align="left">
          <span class="fonts_content_presentation_main"></span>
          <br>
          <strong>Darwinismo digital (infografía): <br>

          </strong>
          <br>
          <a href="../../static/ref/images/005a.jpg" target="_blank"><img src="../../static/ref/images/005a-min.jpg" height="400" width="450"></a>


        </div>

        <!----------------- SLIDE #13 ------------------>
        <div id = "slide13" class="step slide_PDVSA" data-x="13000" data-y="-1500">
          <p align="left">
          <span class="fonts_content_presentation_main"></span>
          <br><br><br>
          <strong>Eso está orientado más a mercadeo, tech-startups y retail <br>
            Estamos en una industria petrolera, <br><br><br>
            y esto es Recursos Humanos!
          </strong>
          <br>
        </div>


        <!----------------- SLIDE #14 ------------------>
        <div id = "slide14" class="step slide_PDVSA" data-x="14000" data-y="-1500">
          <p align="left">
          <span class="fonts_content_presentation_main"></span>
          <br>
          <strong>Futuro digital en la industria petrolera</strong>


          <br>
          <a href="https://www.forbes.com.mx/futuro-digital-en-la-industria-petrolera/" target="_blank"><img src="../../static/ref/images/005b.jpg" height="450" width="800"></a>
        </div>

        <!----------------- SLIDE #15 ------------------>
        <div id = "slide15" class="step slide_PDVSA" data-x="15000" data-y="-1500">
          <p align="left">
          <span class="fonts_content_presentation_main"></span>
          <br>
          <strong>RRHH juega un rol clave en la transformación digital</label></strong>
          <br>
          <br>
          <a href="../../static/ref/docs/HRGV.pdf" target="_blank"><img src="../../static/ref/images/005c.jpg" height="400" width="800"></a>
        </div>

        <!----------------- SLIDE #16 ------------------>
        <div id = "slide16" class="step slide_PDVSA" data-x="16000" data-y="-1500">
          <p align="left">
          <span class="fonts_content_presentation_main"></span>
          <br>
          <strong>Transformación digital de PDVSA y la agilidad</label></strong>
          <br>
          <br>
          <a href="https://solvingadhoc.com/transformacion-digital-empresa-metodos-agiles/" target="_blank"><img src="../../static/ref/images/005d.jpg" height="400" width="800"></a>
        </div>


        <!----------------- SLIDE #17 ------------------>
        <div id = "slide17" class="step slide_PDVSA" data-x="17000" data-y="-1500">
          <p align="left">
          <span class="fonts_content_presentation_main"></span>
          <br><br><br>
          <strong>Experiencia personal:<br><br>
            Esta no es una empresa de tecnología!<br>no es nuestro negocio medular!</label></strong>
          <br>
          <br>
          (Opiniones de un ingeniero)...<br>
          Lectura recomendada:
          <a href="http://www.energiaspatagonicas.com/petroleo/se-viene-la-digital-petrolera-rusia-impulsaria-nuevas-tecnicas-extraccion-latinoamerica/" target="_blank"  class="fonts_main_links">
            El BRICS y su digitalización </a>

          <br>

        </div>



        <!----------------- SLIDE #18 ------------------>
        <div id = "slide18" class="step slide_PDVSA" data-x="18000" data-y="-1500">
          <br>
          <strong>Lecciones de management: Steve Jobs (Apple)</strong>
          <br>
          Las reuniones cortas son importantes: sprint meettings.
          <br><br>

          <video controls id="myvideo" width="420" height="340" controls>
              <source src="../../static/ref/videos/005.mp4">
                Tu navegador no implementa el elemento <code>video</code>.
          </video>

        </div>

        <!----------------- SLIDE #19 ------------------>
        <div id = "slide19" class="step slide_PDVSA" data-x="19000" data-y="-1500">
          <p align="left">
          <span class="fonts_content_presentation_main"></span>
          <br>
          <strong>Cómo sobrevivir?</label></strong>
          <br>
          A través de un enfoque pragmático?
          el pragmatismo suele ser asociado a la practicidad... y las bases teóricas?
          <br><br>
          <table>

              <tr>
              <td><a href="https://www.wikiwand.com/es/John_Dewey" target="_blank"><img src="../../static/ref/images/006a.jpg" height="270" width="220"></a></td>
              <td><a href="https://www.wikiwand.com/es/Charles_Sanders_Peirce" target="_blank"><img src="../../static/ref/images/006b.jpg" height="270" width="220"></a></td>
              <td><a href="https://www.wikiwand.com/es/William_James" target="_blank"><img src="../../static/ref/images/006c.jpg" height="270" width="220"></a></td>
              </tr>
          </table>
        </div>

        <!----------------- SLIDE #20 ------------------>
        <div id = "slide20" class="step slide_PDVSA" data-x="20000" data-y="-1500">





            <br>
            <p align="left">
            <span class="fonts_content_presentation_main"></span>
            <br>
            <strong>El Enfoque Holístico Empresarial</label></strong>

            <br><br>
              La holística empresarial busca integrar equipos de trabajo, bajo una cultura que haga al empleado sentirse parte la empresa,
              promoviendo la superación personal y una mejor calidad de vida, sin dejar de lado la responsabilidad con el ambiente y la sociedad.
              <br>
              Ejemplos: Kabblah para negocios, PNL, Mindfullnes. <br><br>
              Propuesta: mixtura.
              <br>
              Lectura recomendada:<a href="http://journals.sagepub.com/doi/pdf/10.1177/0002764203260208" class="fonts_main_links" target="_blank">
               The Role of Positivity and Connectivity
        </div>


        <!----------------- SLIDE #21 ------------------>
        <div id = "slide21" class="step slide_PDVSA" data-x="21000" data-y="-1500">


          <br>
          <p align="left">
          <span class="fonts_content_presentation_main"></span>
          <br>
          <strong>Críticas al pragmatismo</label></strong>

          <br>
          <a href="http://www.filosofia.org/hem/dep/rcf/n01p024.htm" class="fonts_main_links">Filosofia.org de Cuba </a><br>
          <a href="https://filosofiaparaprofanos.com/2014/10/07/un-politico-pragmatico/" class="fonts_main_links">Filosofía para profanos </a>
<br><br>
            Dejemos de lado el idealismo y vamos a centrarnos en el pragmatismo:
            ¿cuánto tenemos que invertir para modernizar la planta y duplicar la producción?”
            <br>
            <br>

Recomendación: <a href="https://resources.workable.com/blog/tracking-employee-morale" class="fonts_main_links">Medir la felicidad o la "moral del empleado" con Niko-Niko </a><br>

        </div>


<!----------------- SLIDE #22 ------------------>
        <div id = "slide22" class="step slide_PDVSA" data-x="22000" data-y="-1500">

          <p align="left">
          <span class="fonts_content_presentation_main"></span>
          <br>
          <strong>Breaking the rules:
                How Mindfulness, rather than rule-based project management, can benefit certain projects
          </strong><br>
          Project Management Institute (PMI)
          <br>  <br>
          <a href="../../static/ref/docs/003.pdf" target="_blank">
              <img src="../../static/ref/images/010.png" height="250" width="420">
          </a>
          <br>
        </div>
<!----------------- SLIDE #23 ------------------>

        <div id = "slide23" class="step slide_PDVSA" data-x="23000" data-y="-1500">
<br><br>
          <p align="left">
          <span class="fonts_content_presentation_main"></span>
          <br><br>
          <strong>Las metodologías ágiles tienen aproximaciones pragmáticas<br>...y son mesurables
          </strong>
        <br><br>
          Lecturas recomendadas:
          <a href="https://proyectosagiles.org/2008/11/05/por-que-son-buenas-las-demostraciones-en-scrum/" target="_blank" class="fonts_main_links">
              Por qué son buenas las demostraciones en Scrum
          </a>
          <br>
           <a href="../../static/ref/docs/f003.pdf" class="fonts_main_links">
            Falacias de la planificación estratégica  </a><br>

        </div>




<!----------------- SLIDE #24 ------------------>
        <div id = "slide24" class="step slide_PDVSA" data-x="24000" data-y="-1500">
          <br><br>
          <p align="left">
          <span class="fonts_content_presentation_main"></span>
          <strong>Compartir  conocimiento<br>
                  Nueva definición de roles y funciones en la CVP
          </strong><br><br>
          Muchos gerentes, líderes de proyecto y de unidades no comparten la información (Agente Smith)<br><br>
          La rigidez de estos hacen las organizaciones burocráticas y lentas<br>
<br>
Resultado: se dificulta la toma de decisiones!


          <br>
        </div>

<!----------------- SLIDE #25 ------------------>
        <div id = "slide25" class="step slide_PDVSA" data-x="25000" data-y="-1500">
          <br><br>
          <strong>La muerte de un burócrata</strong>

          <br><br>

          <video controls id="myvideo" width="420" height="340" controls>
              <source src="../../static/ref/videos/004.mp4">
                Tu navegador no implementa el elemento <code>video</code>.
          </video>
        </div>

        <!----------------- SLIDE #26 ------------------>
                <div id = "slide26" class="step slide_PDVSA" data-x="26000" data-y="-1500">
                  <br><br>
                  <strong>La agilidad apuesta a:</strong>

                  <br><br>

                  - Un modelo horizontal<br>
                  - Las decisiones no pasan por muchos niveles de autoridad<br>
                  - Controles de Calidad. (W.S.)<br>
                  - Modelos sin jefe, líderes en trabajadores<br>
                  - Valorar la experiencia, individualidad e ideas<br>
                  - Resolver problemas<br>
                  - Tener: misión, visión, valores, cultura organizacional.
                </div>


                <!----------------- SLIDE #27 ------------------>
                        <div id = "slide27" class="step slide_PDVSA" data-x="27000" data-y="-1500">
                          <br>
                          Es esto Anarquismo? Anomia? Bakunin? Von Misses?  DIY?<br>
                          Punks? Comunismo? Igualdad? Ayn Rand y el altruismo?
                          <br><br>
                          <img src="../../static/ref/images/012a.jpg" height="200" width="200">
                          <br><br>
                          Lecturas recomendadas:
                          <br>
                          <a href="http://devmethodologies.blogspot.com/2015/02/scrum-communism.html" target="_blank" class="fonts_main_links">
                              Scrum == Communism ?
                            </a><br>
                          <a href="https://agileanarchy.wordpress.com/scrum-a-new-way-of-thinking/" target="_blank" class="fonts_main_links">
                              Agile Anarchy
                            </a>
                        </div>


                        <!----------------- SLIDE #28 ------------------>
                                <div id = "slide28" class="step slide_PDVSA" data-x="28000" data-y="-1500">
                                  <br>
                                  Oid, mortales, el grito sagrado <br>
                                  de "AGIL" y solidaridad...Viva Viva
                                  <br><br>
                                  <table style="width:50%">

                                      <tr>
                                          <td><img src="../../static/ref/images/012c.jpg" width="270" height="370"></td>
                                      </tr>

                                    </table>

                                </div>


                                <!----------------- SLIDE #29 ------------------>
                                        <div id = "slide29" class="step slide_PDVSA" data-x="29000" data-y="-1500">
                                          <br>  <br>  <br>  <br>  <br>
                                          <strong>Estructuras verticales u horizontales </strong><br>
                                            (no todo es emancipación)
                                        </div>


                                        <!----------------- SLIDE #30 ------------------>
                                                <div id = "slide30" class="step slide_PDVSA" data-x="30000" data-y="-1500">
                                                  <br>  <br>
                                                  <strong>Organizaciones verticales </strong><br><br><br>
                                                    <table style="width:95%">

                                                    <tr>
                                                      <td align="center" bgcolor="#cccc99">
                                                      <strong>  + </strong>
                                                      </td>
                                                      <td align="center" bgcolor="#ff4d4d">
                                                      <strong>  - </strong>
                                                      </td>
                                                    </tr>

                                                    <tr>
                                                      <td align="left" valign="top"><br>
                                                        - Control<br>
                                                        - Reparto de tareas<br>
                                                        - Especialización (T, F)<br><br>

                                                      </td>
                                                      <td align="left" valign="top">
                                                        - Burocracia<br>
                                                        - Coste de salarios<br>
                                                        - Pérdida de oportunidades<br>
                                                        - Desmotivación <br>y frustaciones
                                                      </td>
                                                    </tr>

                                                      </table>
                                                </div>



                                                <!----------------- SLIDE #31 ------------------>
                                                        <div id = "slide31" class="step slide_PDVSA" data-x="31000" data-y="-1500">
                                                          <br>  <br>
                                                          <strong>Organizaciones horizontales </strong><br><br><br>
                                                            <table style="width:95%">

                                                            <tr>
                                                              <td align="center" bgcolor="#cccc99">
                                                              <strong>  + </strong>
                                                              </td>
                                                              <td align="center" bgcolor="#ff4d4d">
                                                              <strong>  - </strong>
                                                              </td>
                                                            </tr>

                                                            <tr>
                                                              <td align="left" valign="top">
                                                                - Satisfacción por la autonomía<br>
                                                                - Cooperación y colaboración<br>
                                                                - Agilidad y adaptación
                                                              </td>
                                                              <td align="left" valign="top">
                                                                - Superposición y solapamiento <br>
                                                                - Escasa promoción<br>
                                                                - Descontrol
                                                              </td>
                                                            </tr>

                                                              </table>
                                                        </div>


          <!----------------- SLIDE #32 ------------------>
                  <div id = "slide32" class="step slide_PDVSA" data-x="32000" data-y="-1500">
                    <br>
                    <strong>Ejercicio práctico #1 </strong><br><br>
                    <br>

                    <iframe src="ejercicio1.php" height="300" width="800" style="border:none;" scrolling="no"></iframe>
                  </div>


                  <!----------------- SLIDE #33 ------------------>
                          <div id = "slide33" class="step slide_PDVSA" data-x="33000" data-y="-1500">
                            <br>  <br>
                            <strong>Lecciones aprendidas: </strong><br><br>
                            - Rapidez<br>
                            - Trabajar bajo presión<br>
                            - Iteraciones<br>
                            - Ensayo y error<br>
                            - Autogestión
                          </div>



                          <!----------------- SLIDE #34 ------------------>
                          <div id = "slide34" class="step slide_PDVSA" data-x="34000" data-y="-1500">
                          <br><br><br><br><br>
                          <strong>
                          Definiciones de proyecto y proceso
                        </strong>
                          <br>
                        </div>


                        <!----------------- SLIDE #35 ------------------>
                        <div id = "slide35" class="step slide_PDVSA" data-x="35000" data-y="-1500">
                        <br><br><br>
                        <strong>
                          Proyecto:
                        <br>
                        </strong>
                        <br>
                          - Conjunto de actividades relacionadas y ejecutadas para cumplir un objetivo<br>
                          - El objetivo puede ser un producto o servicio, siempre único<br>
                          - Es temporal<br>
                          - Alcance y recursos limitados<br>
                          - No son una operación rutinaria


                      </div>


                      <!----------------- SLIDE #36 ------------------>
                      <div id = "slide36" class="step slide_PDVSA" data-x="36000" data-y="-1500">
                      <br><br><br>
                      <strong>
                        Procesos:
                      <br>
                      </strong>
                      <br>
                      - No es único<br>
                      - Obtiene entregables iguales o parecidos<br>
                      - Sus actividades no se realizan en fechas concretas <br>
                      - Tiene un carácter estático<br>
                      - No gestiona bien las incertidumbres
                    </div>


                    <!----------------- SLIDE #37 ------------------>
                    <div id = "slide37" class="step slide_PDVSA" data-x="37000" data-y="-1500">
                        <br><br><br>
                        <strong>Convirtiendo un proyecto en proceso ó proceso en proyecto?</strong>
                        <br><br>
                        - Al obtener éxito<br>
                        - Pasa a ser un referente<br>
                        - Estandarizamos y reutilizamos<br>
                        - Convertir una oportunidad en proyecto
                        <br><br>
                        <strong>Resultado:</strong> Enriquecemos a nuestra organización
                        <br><br>
                    </div>


                    <!----------------- SLIDE #38 ------------------>
                    <div id = "slide38" class="step slide_PDVSA" data-x="38000" data-y="-1500">
                        <br><br><br><br><br>
                        <strong>
                          ¿Como se gestionaban los proyectos en la antigüedad?
                        </strong>
                    </div>

                    <!----------------- SLIDE #39 ------------------>
                    <div id = "slide39" class="step slide_PDVSA" data-x="39000" data-y="-1500">
                        <br><br>
                        <strong>
                          La división del trabajo para la Necrópolis de Giza
                        </strong>
                        <br>
                        <br>
                        <img src="../../static/ref/images/013.jpg" height="350" width="720">
                    </div>


                    <!----------------- SLIDE #40 ------------------>
                    <div id = "slide40" class="step slide_PDVSA" data-x="40000" data-y="-1500">
                        <br><br>
                        <strong>
                        Los proyectos griegos  y romanos 600 años A.C
                        </strong>
                        <br>
                        <br>
                        <video controls id="myvideo" width="420" height="340" controls>
                            <source src="../../static/ref/videos/007.mp4"></source>
                        </video>
                    </div>


                    <!----------------- SLIDE #41 ------------------>
                    <div id = "slide41" class="step slide_PDVSA" data-x="41000" data-y="-1500">
                        <br>
                        <strong>
                          Basílica de Santa Sofía Madre en Turquía 365 D.C
                        </strong>
                        <br>
                        <br>
                        <img src="../../static/ref/images/014.jpg" height="380" width="620">
                    </div>


                    <!----------------- SLIDE #42 ------------------>
                    <div id = "slide42" class="step slide_PDVSA" data-x="42000" data-y="-1500">
                        <br>
                        <strong>
                          Castillo de Bodiam en Inglaterra
                        </strong>
                        <br>
                        <br>
                        <img src="../../static/ref/images/015.jpg" height="380" width="620">
                    </div>


                    <!----------------- SLIDE #43 ------------------>
                    <div id = "slide43" class="step slide_PDVSA" data-x="43000" data-y="-1500">
                        <br><br>
                        <strong>Denominadores comunes de los grandes proyectos</strong>
                        <br><br>
                          - Guerras<br>
                          - Religión y etnicidad<br>
                          - Opresión, defensa  y la conquista<br>
                          - Recursos y poder<br>
                          <br>
                          <br>
                          - Riqueza...
                        <br>
                    </div>


                    <!----------------- SLIDE #44 ------------------>
                    <div id = "slide44" class="step slide_PDVSA" data-x="44000" data-y="-1500">
                        <br><br><br><br><br>
                        <strong>Qué es una revolución industrial y cuantas se han dado?</strong>
                        <br><br>
                        <strong>Estamos en una revolución industrial?</strong>
                        <br>
                    </div>


                    <!----------------- SLIDE #45 ------------------>
                    <div id = "slide45" class="step slide_PDVSA" data-x="45000" data-y="-1500">
                        <br><br>
                        <strong>1era Revolución industrial/minera (1760 - 1840 aprox) </strong>
                        <br><br>
                        <strong>Características</strong>
                        <br>- Se origina en Gran Bretaña
                        <br>- Cambio radical de la sociedad, política, economía y religiosa
                        <br>- Aparece la máquina de vapor (Alejandría/Watt)
                        <br>- Doble revolución industrial (laicismo Francés)
                        <br><br>
                        <strong>Antecedentes: </strong>Trabajo agrario, baja productividad, aparición de nuevos materiales y fuentes de energía
                    </div>

                    <!----------------- SLIDE #46 ------------------>
                    <div id = "slide46" class="step slide_PDVSA" data-x="46000" data-y="-1500">
                      <br><br>

                      <strong>Ventajas y desventajas</strong><br><br>
                      <table style="width:95%">
                      <tr>
                          <td align="center" bgcolor="#cccc99">
                          <strong>  + </strong>
                          </td>
                          <td align="center" bgcolor="#ff4d4d">
                          <strong>  - </strong>
                          </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top"><br>
                        - Nuevos métodos de trabajo<br>
                        - Progreso Tecnológico<br>
                        - Nuevas formas de comercialización

                        </td>
                        <td align="left" valign="top">

                        - Nuevas clases sociales<br>
                        - Opresión e injusticias<br>
                        - Migraciones<br>
                        </td>
                    </tr>

                    </table>
                    </div>


                    <!----------------- SLIDE #47 ------------------>
                    <div id = "slide47" class="step slide_PDVSA" data-x="47000" data-y="-1500">
                      <br><br>
                      <strong>Adam Smith (1723-1790)</strong>
                      <br><br>
                      - Nació en Escocia<br>
                      - Primer economista moderno<br>
                      - La Riqueza de las Naciones y las leyes naturales del mercado <br>
                      - Economía libre, por intereses, individualista y competititiva <br><br>
                      - <strong>Puntos clave:</strong> división del trabajo y la productividad (el alfiler)
                    </div>

                    <!----------------- SLIDE #48 ------------------>
                    <div id = "slide48" class="step slide_PDVSA" data-x="48000" data-y="-1500">
                      <br>
                      <strong>
                        Fundamentos y ejemplos de Adam Smith
                      </strong>
                      <br><br>
                      <video controls id="myvideo" width="420" height="340" controls>
                          <source src="../../static/ref/videos/008.mp4"></source>
                      </video>
                    </div>


                    <!----------------- SLIDE #49 ------------------>
                    <div id = "slide49" class="step slide_PDVSA" data-x="49000" data-y="-1500">
                      <br><br>
                        <strong>2da Revolución industrial (1870 - 1945 aprox)  </strong>
                        <br><br>

                        - Más innovaciones, más tecnología (sistemas de transporte, industria) y
                         nuevas fuentes de energía <br>
                        - Cambios organizacionales, económicos y de mercado<br>
                        - Aparece el capitalismo y la primera globalización <br>
                        - Las dos guerras mundiales<br>
                        - Explosión demográfica<br>
                    </div>

                    <!----------------- SLIDE #50 ------------------>
                    <div id = "slide50" class="step slide_PDVSA" data-x="50000" data-y="-1500">
                      <br><br>
                        <strong>Frederick Taylor y la gestión de procesos  </strong>
                        <br><br>

                        - Padre de la administración científica<br>
                        - Estudios del trabajo, el razonamiento científico aplicado al trabajo<br>
                        - Menos trabajo, menos esfuerzo y menos tiempo<br><br>
                         Otros: Fayol y Ford<br><br>

                    </div>

                    <!----------------- SLIDE #51 ------------------>
                    <div id = "slide51" class="step slide_PDVSA" data-x="51000" data-y="-1500">
                      <br><br>
                      <strong>
                       Resumen de Taylor y su importancia
                      </strong>
                      <br><br>
                      <video controls id="myvideo" width="420" height="340" controls>
                          <source src="../../static/ref/videos/009.mp4"></source>
                      </video>

                    </div>


                    <!----------------- SLIDE #52 ------------------>
                    <div id = "slide52" class="step slide_PDVSA" data-x="52000" data-y="-1500">
                      <br><br><br>
                      <strong>
                       Taylor VS Ágil
                      </strong>
                      <br><br>
                      - Poca atención al elemento humano<br>
                      - Superespecialización del operario<br>
                      - Enfoque incompleto de la organización<br>
                      - Enfoque de sistema cerrado y mecanicista
                    </div>

                    <!----------------- SLIDE #53 ------------------>
                    <div id = "slide53" class="step slide_PDVSA" data-x="53000" data-y="-1500">
                      <br><br>
                        <strong>3ra Revolución industrial (1950 - actualidad)  </strong>
                        <br><br>

                        La Tercera Revolución Industrial es la Revolución Científico-Técnica o Revolución de la Inteligencia
                        <br><br>
                        Se caracteriza por:
                        <br>

                        - La microelectrónica<br>
                        - Tecnologías avanzadas<br>
                        - Sectores centrados en I+D
                    </div>

                    <!----------------- SLIDE #54 ------------------>
                    <div id = "slide54" class="step slide_PDVSA" data-x="54000" data-y="-1500">
                      <br><br>
                        <strong>4ta Revolución industrial  </strong>
                        <br><br>

                        VIDEO
                    </div>


                    <!----------------- SLIDE #55 ------------------>
                    <div id = "slide55" class="step slide_PDVSA" data-x="55000" data-y="-1500">
                      <br>
                        <strong>Visión World Economic Forum </strong>
                        <br><br>
                        <a href="https://www.weforum.org/es/agenda/archive/fourth-industrial-revolution/" target="_blank">
                        <img src="../../static/ref/images/019.jpg" width="750" height="400">
                      </a>

                    </div>


                    <!----------------- SLIDE #56 ------------------>
                    <div id = "slide56" class="step slide_PDVSA" data-x="56000" data-y="-1500">
                      <br>
                        <strong>5 formas de usar AI en RRHH</strong>
                        <br><br>
                        <a href="http://bigdata-madesimple.com/5-ways-to-use-artificial-intelligence-ai-in-human-resources/" target="_blank">
                        <img src="../../static/ref/images/020.jpg" width="750" height="400">
                      </a>

                    </div>


                    <!----------------- SLIDE #57 ------------------>
                    <div id = "slide57" class="step slide_PDVSA" data-x="57000" data-y="-1500">
                      <br>
                        <strong>Agile + AI + RRHH</strong>
                        <br><br>
                        <a href="https://www.employeeconnect.com/blog/artificial-intelligence-hr/" target="_blank">
                        <img src="../../static/ref/images/021.jpg" width="750" height="400">
                      </a>

                    </div>

                    <!----------------- SLIDE #58 ------------------>
                    <div id = "slide58" class="step slide_PDVSA" data-x="58000" data-y="-1500">

                      <br>
                      <br>
                        <strong>Ejercicio práctico 2</strong>
                        <br>
                        <br>
                      <iframe src="ejercicio2.php" height="300" width="800" style="border:none;" scrolling="no"></iframe>
                    </div>


                    <!----------------- SLIDE #59 ------------------>
                            <div id = "slide59" class="step slide_PDVSA" data-x="59000" data-y="-1500">
                              <br>  <br>
                              <strong>Lecciones aprendidas: </strong><br><br>
                              - Fast Sprint meetings (10mins o menos)<br>
                              - Aplicar creatividad e innovar<br>
                              - Experimentar y enfrentarse a la entropía<br>
                              - Esto trata de adaptarse con apoyo mutuo<br><br>
                              Lectura recomendada:<br>
                              <a href="../../static/ref/docs/005.pdf" target="_blank" class="fonts_main_links">
                              Para escalar procesos ágiles no se puede improvisar
                              </a>

                            </div>



                    <!----------------- SLIDE #60 ------------------>
                    <div id = "slide60" class="step slide_PDVSA" data-x="60000" data-y="-1500">
                      <br>
                      <strong>Henry Gantt</strong>
                      <br><br>
                      <img src="../../static/ref/images/016.png" width="750" height="330">


                    </div>

                    <!----------------- SLIDE #61 ------------------>
                    <div id = "slide61" class="step slide_PDVSA" data-x="61000" data-y="-1500">
                      <br>
                      <strong>El gráfico de Gantt</strong>
                      <br><br>
                      Herramienta visual para la planificación y
                      programación de actividades o tareas sobre una línea del tiempo.<br><br>
                      Permite al usuario establecer la duración y el comienzo de cada actividad.
                      A través de una gráfica, fácil de interpretar,
                      el usuario puede llevar un control de la planificación de su trabajo.
                    </div>

                    <!----------------- SLIDE #62 ------------------>
                    <div id = "slide62" class="step slide_PDVSA" data-x="62000" data-y="-1500">
                      <br>
                      <strong>Mitos y verdad del gráfico de Gantt</strong>
                      <br>
                      - Uso Gantt en mi vida diaria? O es un formalismo documental<br>
                      - Un Diagrama de Gantt  se queda obsoleto contínuamente<br>
                      - Lo utilizabamos para “estimar” nuestro  trabajo con Ms. Project<br><br>
                      <a href="../../static/ref/images/018.jpg" target="_blank">
                      <img src="../../static/ref/images/018.jpg" width="370" height="250">
                      </a>
                    </div>

                    <!----------------- SLIDE #63 ------------------>
                    <div id = "slide63" class="step slide_PDVSA" data-x="63000" data-y="-1500">
                      <br>
                      <strong>Waterfall y modelo predictivo (Wiston Royce)</strong>
                      <br>

                      <br>

                      <img src="../../static/ref/images/022.png" width="370" height="250">

                      <br><br>
                      Lecturas recomendadas:
                      <a href="../../static/ref/docs/006.pdf" target="_blank" class="fonts_main_links"><br>
                      Documento del Departamento de defensa de E.E.U.U
                    </a>

                    <a href="https://pragtob.wordpress.com/2012/03/02/why-waterfall-was-a-big-misunderstanding-from-the-beginning-reading-the-original-paper/" target="_blank" class="fonts_main_links"><br>
                    Why Waterfall Was a Big misunderstanding From The Beginning
                  </a>

                    </div>

                    <!----------------- SLIDE #64 ------------------>
                    <div id = "slide64" class="step slide_PDVSA" data-x="64000" data-y="-1500">
                      <br>
                      <strong>La complejidad de un proyecto puede depender de diferentes factores como, por ejemplo:</strong>
                      <br><br>
                      <img src="../../static/ref/images/023.gif" width="550" height="380">

                    </div>



                    <!----------------- SLIDE #65 ------------------>
                    <div id = "slide65" class="step slide_PDVSA" data-x="65000" data-y="-1500">
                      <br>
                      <strong>Control empírico e incremental</strong>
                      <br><br>
                      <img src="../../static/ref/images/025.gif" width="550" height="380">

                    </div>
                    <!----------------- SLIDE #66 ------------------>
                    <div id = "slide66" class="step slide_PDVSA" data-x="66000" data-y="-1500">
                      <br>
                      <strong>Control empírico e incremental (detalle)</strong>

                      <br><br>
                      <img src="../../static/ref/images/026.gif" width="550" height="380">

                    </div>

                    <!----------------- SLIDE #67 ------------------>
                    <div id = "slide67" class="step slide_PDVSA" data-x="67000" data-y="-1500">
                      <br>
                      <strong>Walter Shewhart (1891-1967)</strong>
                      <br><br>

                      Primero en hablar de las mejoras contínuas


                      <br><br>
                      <img src="../../static/ref/images/027.jpg" width="260" height="260">

                      <br><br>
                      Lectura recomendada:
                      <a href="https://qualityandinnovation.com/2011/08/26/pdca-vs-pdsa-what%E2%80%99s-the-difference/" target="_blank" class="fonts_main_links">
                      PDCA vs. PDSA: What’s the Difference?
                      </a>


                    </div>

                    <!----------------- SLIDE #68 ------------------>
                    <div id = "slide68" class="step slide_PDVSA" data-x="68000" data-y="-1500">
                      <br><br>
                      <strong>PMI Project Management Institute</strong><br><br>
                      <img src="../../static/ref/images/pmi.gif" width="720" height="320">



                    </div>


                    <!----------------- SLIDE #69 ------------------>
                    <div id = "slide69" class="step slide_PDVSA" data-x="69000" data-y="-1500">
                      <br><br><br><br><br>
                      <strong>Métodos de productividad personal / laboral</strong>
                      <br><br>



                    </div>


                    <!----------------- SLIDE #70 ------------------>
                    <div id = "slide70" class="step slide_PDVSA" data-x="70000" data-y="-1500">
                      <br><br>
                      <strong>Pomodoro</strong>
                      <br><br>
                      La técnica del pomodoro consiste en dividir nuestro tiempo de trabajo en períodos de 25 minutos a los que dedicaremos una tarea.
                      <br><br>
                      Una vez finalizado dicho periodo se hará un breve descanso de 5 minutos y así hasta completar 4 pomodoros. Cuando hayamos acabado el 4º, el descanso será de 15 minutos.



                    </div>


                    <!----------------- SLIDE #71 ------------------>
                    <div id = "slide71" class="step slide_PDVSA" data-x="71000" data-y="-1500">

                      <br><br>
                      <strong>Aplicaciones Pomodoro recomendadas:</strong><br><br>
                      <table style="width:100%">

                          <tr>
                              <td><img src="../../static/ref/images/pomodoro1.jpg" width="300" height="300"></a>
                              <br> Focus Booster

                              </td>
                              <td><img src="../../static/ref/images/pomodoro2.jpg" width="300" height="300"></a>
                              <br> Flat Tomato

                              </td>
                          </tr>

                        </table>


                    </div>


                    <!----------------- SLIDE #72 ------------------>
                    <div id = "slide72" class="step slide_PDVSA" data-x="72000" data-y="-1500">
                      <br><br>
                      <strong>Getting Things Done</strong>
                      <br><br>
                      Getting Things Done apuesta por tener un sistema organizativo con el que podamos confiar,
                      externo a nuestra memoria, que nos permita acordarnos de las cosas que tenemos que acordarnos en el momento que tenemos que acordarnos.
                      <br><br>Se basa en principios sencillos, que todos podemos dar como ciertos.

                    </div>

                    <!----------------- SLIDE #73 ------------------>
                    <div id = "slide73" class="step slide_PDVSA" data-x="73000" data-y="-1500">
                      <br><br>
                      <strong>Getting things Done. 5 pasos para hacerlo efectivo</strong><br><br>
                      1. Recopilación<br>
                      2. Procesamiento<br>
                      3. Organización<br>
                      4. Revisión<br>
                      5. Ejecución<br><br>

                      Lectura recomendada:
                      <a href="https://www.sintetia.com/los-beneficios-de-incorporar-gtd-a-las-empresas/" target="_blank" class="fonts_main_links"><br>
                      Los beneficios de incorporar GTD a las empresas
                    </a>



                    </div>


                    <!----------------- SLIDE #74 ------------------>
                    <div id = "slide74" class="step slide_PDVSA" data-x="74000" data-y="-1500">
                      <br><br>
                      <strong>Aplicaciones GTD recomendadas:</strong><br><br><br>

                      <table style="width:100%">

                          <tr>
                              <td><img src="../../static/ref/images/gtd1.png" width="200" height="200"></a>
                              <br> Remember the milk

                              </td>
                              <td><img src="../../static/ref/images/gtd2.png" width="200" height="200"></a>
                              <br> Todoist

                              </td>

                              <td><img src="../../static/ref/images/gtd3.png" width="220" height="220"></a>
                              <br> ZenDone

                              </td>
                          </tr>

                        </table>
                    </div>

                      <!----------------- SLIDE #75 ------------------>
                    <div id = "slide75" class="step slide_PDVSA" data-x="75000" data-y="-1500">
                        <br>
                        <strong>Caso a considerar para Ágil: Japón</strong>
                        <br>

                        https://www.youtube.com/watch?v=0vIoqSGhkYE

                        <br>
                  </div>

                  <!----------------- SLIDE #76 ------------------>
                  <div id = "slide76" class="step slide_PDVSA" data-x="76000" data-y="-1500">
                      <br><br>
                  <strong> La teoría Z</strong>
                    <br>  <br>
                   - Desarrollada por William Ouchi  <br>
                   - Liderazgo consistente en determinar la naturaleza de la mujer y el hombre  <br>
                   - Apoyada por Fromm y Drucker  <br>
                   - Basada en la confianza, sutileza e intimidad y 14 pasos
                   <br><br>
                   Lectura recomendada: <a href="../../static/ref/docs/008.pdf" target="_blank" class="fonts_main_links">
                   ¿Qué tan “Z” es la teoría “Z” de Ouchi?
                 </a>
                  </div>
<!--

                  <div id = "slide77" class="step slide_PDVSA" data-x="77000" data-y="-1500">
                      <br><br><br><br><br><br>
                      <strong>Ejercicio #1: The Mashmellow Challenge</strong>

                 </a>
                  </div>




                  <div id = "slide78" class="step slide_PDVSA" data-x="78000" data-y="-1500">
                      <br><br>
                  <strong> Lean</strong>
                    <br>  <br>
                   - Desarrollada por Sakichi Toyoda (Fundador de Toyota)  <br><br>
                   - Busca la automatización del trabajo en donde las máquinas, instalaciones y personas trabajan juntos<br><br>
                   - Establece el modelo Kanban con iteraciones ajustables continuamente, reducción de documentación y comunicación constante<br>

                 </a>
                  </div>



                  <div id = "slide79" class="step slide_PDVSA" data-x="79000" data-y="-1500">
                    <br><br>
                <strong> Lean (cont)</strong>
                  <br>  <br>
                    - Evitar Muri (sobrecarga) y mura (errores de balance en función del proyecto)<br><br>
                    - El Gobierno japonés extendió el modelo a otras industrias para fomentar la ventaja competitiva
                  </div>


                  <div id = "slide80" class="step slide_PDVSA" data-x="80000" data-y="-1500">
                    <br><br>
                <strong> Lean</strong><br>Principios básicos
                  <br>  <br>
                 <strong>1. Eliminar el desperdicio:</strong> o lo que no aporta valor (Muda 無駄) <br>
                 <strong>2. Crear conocimiento:</strong> cultura de mejora contínua, métodos de resolución de problemas  <br>
                 <strong>3. Crear la calidad: </strong> automatizar, refactorizar, estar al día <br>
                 <strong>4. Tomar decisiones lo más tarde: </strong> user stories cercanas a la realidad <br>

                   Resultado: <strong>JUST IN TIME</strong>!
                  </div>



                  <div id = "slide81" class="step slide_PDVSA" data-x="81000" data-y="-1500">
                      <br><br>
                  <strong> Lean</strong><br>Principios básicos (cont)
                    <br>  <br>
                   <strong>5. Entregar ASAP:</strong> basado en impacto, valor y prioridades <br>
                   <strong>6. Potenciar el equipo:</strong> toma de decisiones, estimaciones, complejidades  <br>
                   <strong>7. Visualizar el todo: </strong> Pensar en grande, actuar en pequeño, equivocarse y aprender con rapidez
                   <br>
                   <br>
                   Resultado: <strong>JUST IN TIME</strong>!
                  </div>



                  <div id = "slide82" class="step slide_PDVSA" data-x="82000" data-y="-1500">
                      VIdeo
                  </div>




                  <div id = "slide83" class="step slide_PDVSA" data-x="83000" data-y="-1500">
                      oneg(k)aishimasu | arig(k)atugoz(s)aimasu | konnichiw(h)a  kanb(h)an!!!<br>
                      <img src="../../static/ref/images/023.png" width="700" height="500">
                      <br>


                  </div>

                  <div id = "slide84" class="step slide_PDVSA" data-x="84000" data-y="-1500">
                      <br><br> <br><strong>En Hiragana ( no es Kanji ni Katakana)</strong>
                  <br><br>
                          Por favor > onegaishimasu > おねがいします<br>
                          Gracias > arigatougozaimasu > ありがとうございます
                          <br><br><br><br>

                          Hola > konnichiwa > こんにちは"  <br>
                          Kanban > kanban > かんばん</strong>
                   <br>
                   <br>

                  </div>


                  <div id = "slide85" class="step slide_PDVSA" data-x="85000" data-y="-1500">

                <br><br><br><br><br>
                <strong>
                        Kanban<br></strong>
                        かんばん<br>
                        看板


                 <br>
                 <br>

                  </div>


                  <div id = "slide86" class="step slide_PDVSA" data-x="86000" data-y="-1500">

                <br><br><br><br>
                <strong> Claves en Kaban:<br></strong>
                - Nos da entendimiento de cómo funciona nuestra manera de trabajar, cuanto podemos manejar
                al mismo tiempo y con qué calidad haremos entregas.<br>
                - Nos ayuda a ser más predictivos y trabajar a ritmo sostenido<br>
                - Se pontencia la comunicación, el respeto y la colaboración, por ende la calidad <br>
                - Se desarrolla un entendimiento innato en la gestión del riesgo<br>
                </div>


                <div id = "slide87" class="step slide_PDVSA" data-x="87000" data-y="-1500">

                <br><br><br><br>
                <strong> Claves en Kaban:</strong><br>
                - Da entendimiento de cómo funciona nuestra manera de trabajar, cuanto se maneja
                al mismo tiempo y con qué calidad se hacen entregas.<br>
                -  Ayuda a ser más predictivos y trabajar a ritmo sostenido<br>
                - Se pontencia la comunicación, el respeto y la colaboración, por ende la calidad <br>
                - Se desarrolla un entendimiento innato en la gestión del riesgo<br>
                </div>

                <div id = "slide88" class="step slide_PDVSA" data-x="88000" data-y="-1500">

                <br><br><br><br>
                <strong> Claves en Kaban (cont):<br></strong>
                - Se obtiene un alineamiento del negocio, permitiendo alcanzar objetivos estratégicos<br>
                - Enfoca el compromiso gestionado y flujos equilibrados de trabajo.
                - Ofrece la posibilidad de cambiar el rumbo si cambian las condiciones o emergen nuevas dependencias<br>
                - Se genera la manofactura esbelta y nuevos fundamentos de calidad
                </div>


                <div id = "slide89" class="step slide_PDVSA" data-x="89000" data-y="-1500">

                <br><br>
                <strong> Historia de Kaban <br></strong>
                - Detonadores: economía de postguerra y la búsqueda de eficiencia en la gestión de procesos<br>
                - Su origen real está en los errores en que caían los  automercados norteamericanos<br>
                  y su descrontol al momento de reponer inventarios<br>
                - Se añada el sistema PULL (jalar) que consiste en hacer una orden y ejecutarla tan pronto haya sido emitida<br>
                - Se perfecciona en Toyota gracias a Taiichi Onho junto con Eiji Toyoda en 1958
                </div>

                <div id = "slide90" class="step slide_PDVSA" data-x="90000" data-y="-1500">

                <br><br><br>
                <strong> Historia de Kaban </strong>
                - Detonadores: economía de postguerra y la búsqueda de eficiencia en la gestión de procesos<br>
                - Su origen real está en los errores en que caían los  automercados norteamericanos<br>
                  y su descrontol al momento de reponer inventarios<br>
                - Se añada el sistema PULL (jalar) que consiste en hacer una orden y ejecutarla tan pronto haya sido emitida<br>
                - Se prefeccionan activos intangibles convirtiendose en valor
                </div>


                <div id = "slide91" class="step slide_PDVSA" data-x="91000" data-y="-1500">

                <br><br>
                <table style="width:80%">

                  <br><br>
                  <strong> Valores de Kaban (empieza por donde estés) </strong>
                  <table>
                    <tr>
                        <td>Transparencia</td>
                        <td>Equilibrio</td>
                    </tr>
                        <td>Colaboración</td>
                        <td>Foco en el cliente</td>
                    <tr>
                        <td>Flujo</td>


                          <td>Liderazgo</td>
                          <td>Entendimiento</td>
                          <td>Acuerdo</td>
                          <td>Respeto</td>
                        </tr>
                  </table>



                </div>

                <div id = "slide92" class="step slide_PDVSA" data-x="92000" data-y="-1500">

                  <br><br>    <br><br>
                  <strong>La repostería Kanban</strong><br>
                  1era iteración<br><br>
                </div>

                <div id = "slide93" class="step slide_PDVSA" data-x="93000" data-y="-1500">

                  <br><br>La repostería Kanban. Términos clave: </strong>
                  <br>Sprints o iteraciones, historias, retrospectivas, buffer, bloqueos e impedimientos <br><br>
                  <strong> Métricas: </strong><br>
                  - Throughput o  Rendimiento / Lead Time o Tasa de Entrega<br>
                  - TiP (Time in Progress) o Tiempo en progreso<br>
                  - WiP (Work in progress) o Trabajo en proceso<br>
                </div>

                <div id = "slide94" class="step slide_PDVSA" data-x="94000" data-y="-1500">

                  <br><br>
                  <strong> Ejercicio práctico: La repostería Kanban.</strong><br>
                   Retrospectiva<br><br>
                  - Flujos de trabajo atascados?
                  - Cómo solucionar errores y alcanzar mayor productividad<br><br>
                  Hagamos más iteraciones

                </div>

                <div id = "slide95" class="step slide_PDVSA" data-x="95000" data-y="-1500">
                  <br>
                  <strong> Métricas y Ley de Little</strong><br>
                  Total tasa de entregas o Redimiento = WiP/TiP

                  <br><br>
                  La ley de Little se puede mostrar gráficamente en un diagrama de flujo acumulado y nos permitirá ver cuantitativamente nuestro desempeño
                  <table>
                    <tr>
                        <td>
                          <a href="../../static/ref/images/f001.png" target="_blank"><img src="../../static/ref/images/f001.png" width="400" height="250"></a>
                        </td>
                        <td><a href="../../static/ref/images/f002.jpg" target="_blank"><img src="../../static/ref/images/f002.jpg" width="400" height="250"></a>
                        </td>
                    </tr>
                  </table>
                </div>


                <div id = "slide96" class="step slide_PDVSA" data-x="96000" data-y="-1500">

                  <br>
                  <strong> A conversar:</strong><br>
                  Sugerencias en la gestión actual por procesos en RRHH:

                  <br><br>
                  - Un archivo en Ms. Excel compartido por múltiples usuarios <br>
                  - Datos no son consistentes <br>
                  - No existe una métrica real de trabajos ejecutados y se pierde el objetivo gerencial<br>
                  - Hay actividades emergentes? atascos en el comité de RRHH y en las firmas (esto no se cuantifica)<br><br>
                  ...y Jessika Bottini? y Florangel Esparragoza?
                </div>

                <div id = "slide97" class="step slide_PDVSA" data-x="97000" data-y="-1500">

                  <br><br><br><br>
                  <strong> Previsión y métricas</strong>
                  Enfoque de Pensamiento de Sistemas para intoducir Kanban <br>
                  Systems Thinking Approach To Implementing Kanban(STATIK)
                  <br><br>
                  Suboptimización aislada de procesos para posteriormente ver el "todo" para mejorar el flujo de valor al cliente. <br>
                  <br>Caso real: Jubilaciones semestrales / beneficios al personal
                </div>

                <div id = "slide98" class="step slide_PDVSA" data-x="98000" data-y="-1500">

                  <br>
                  <strong> Previsión y métricas<br>
                  Previsión probabilistica de flujo de valor a través del Método Montecarlo.</strong>
                  <br><br>Se necesita:

                  <br>
                  - Datos de la variablidad de elementos o factores externos que afecten el desempeño.<br>
                  - Tiempos y tasas de entrega<br>
                  - Estimaciones si no se disponen de datos reales (recordemos que es una simulación)
                  - Esto estaría precargado y a través del trabajado diario se recolectarían los datos
                  de cada trabajador

                </div>

                <div id = "slide99" class="step slide_PDVSA" data-x="99000" data-y="-1500">

                  <br><br>
                  <strong> Previsión y métricas (cont)<br>
                  Previsión probabilistica de flujo de valor a través del Método Montecarlo.</strong>
                  <br><br>Se obtiene:

                  <br>
                  - Porcentajes de probabilidad de intervalos de fecha de terminación o entregas
                  - Una mejor planificación enfocada a eliminar costes y riesgos con agendas y compromisos<br>
                  Lectura recomendada:
                  <a href="http://www.sc.ehu.es/sbweb/fisica_/numerico/montecarlo/montecarlo.html" target="_blank" class="fonts_main_links"><br>
                  El método Montecarlo
                </a>
                </div>


                <div id = "slide100" class="step slide_PDVSA" data-x="100000" data-y="-1500">

                  <br><br><br><br><br>
                  <strong> Kanban en RRHH-CVP.  Caso: Captación y empleos<br>
                  Flujograma del proceso facilitado por la Sta. Taybert López<br>
                <a href="../../static/ref/docs/f003.pdf" target="_blank"><img src="../../static/ref/images/docs/f003.pdf" class="fonts_main_links"></a>

                </div>

                <div id = "slide101" class="step slide_PDVSA" data-x="101000" data-y="-1500">

                  <br><br>
                  <strong> Métodos Ágiles en la vida diaria<br></strong>
                  Charla TED de como usar Kanban para "gestionar" una familia<br><br>
                  <video controls id="myvideo" width="420" height="340" controls>
                      <source src="../../static/ref/videos/003.mp4"></source>
                  </video>
                  <br><br>Discutamos acerca de esto
                </a>
                </div>


                <div id = "slide102" class="step slide_PDVSA" data-x="102000" data-y="-1500">

                  <br><br>
                  <strong> Métodos lúdicos de estimación<br></strong>
                  El Póker Planning
                  <br><br>
                  - Se usa para estimaciones de tiempo<br>
                  - Organiza los user stories<br>
                  - Basado en Fibonacci: 1 , 1 , 2 , 3 , 5 , 8 , 13 , 21 , 34 , 55 , 89 , 144
                </a>
                </div>



                <div id = "slide103" class="step slide_PDVSA" data-x="103000" data-y="-1500">

                  <br><br>
                  <strong> Valores y principios ágiles<br></strong>

                  <br>Lectura obligada:<br>
                  <a href="http://agilemanifesto.org/iso/es/manifesto.html" target="_blank">El Manifiesto Ágil</a>

                </a>
                </div>


                <div id = "slide104" class="step slide_PDVSA" data-x="104000" data-y="-1500">

                  <br><br>
                  <strong> Para la próxima sesión:<br> Agilidad en el Contexto de RRHH II<br></strong>

                  <br><br>
                  - Profundización de roles y conceptos ágiles<br>
                  - Scrum: userstories, estimaciones, restropectivas, sprints<br>
                  - Dinámicas grupales<br>
                  - Otros marcos <br>
                  - Aplicabilidad en RRHH y otras gerencias dentro de CVP<br>
                </a>
                </div>
-->
                <div id = "slide77" class="step slide_PDVSA" data-x="77000" data-y="-1500">

                  <br><br><br><br>
                  <strong> Be Agile...</strong>


                </div>



<div class="hint">
    <p>Presione espaciadora</p>
</div>
<script>
    if ("ontouchstart" in document.documentElement) {
        document.querySelector(".hint").innerHTML = "<p>Presione las flechas izquierda y/o derecha para retroceder/avanzar</p>";
    }
</script>


<script>impress().init();</script>

</div>

    <div class="footer_main">

        <?php include '../includes/footer_presentation.html';?>
    </div>



</body>
</html>
