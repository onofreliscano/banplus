<!DOCTYPE html>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
 
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" >
 
<link rel="stylesheet" href="styles.css" >
 
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<html class="no-js" lang="en">
<head>
    <title>Sistema de RRHH Corporativo</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">




</head>



    <body class="impress-not-supported" ng-app >


        <form class="form-signin" method="POST">
           <h2 class="form-signin-heading">Sistema de RRHH Corporativo</h2>
           <div class="input-group">
	           <span class="input-group-addon" id="basic-addon1">Nombre de usuario:</span>
	           <input type="text" name="username" class="form-control" placeholder="Username" required>
	       </div>
           <br>
           <label for="inputPassword" class="sr-only">Contraseña:</label>
           <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
           <button class="btn btn-lg btn-primary btn-block" type="submit">Acceder</button>
           <a class="btn btn-lg btn-primary btn-block" href="register.php">Register</a>
      </form>
    </body>

</html>